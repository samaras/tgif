var express = require('express');
var passport = require('passport');
var router = express.Router();

/* GET trays for user. */
router
	.get('/api/trays/users/:id',  isAuthenticated, function (req, res) {
  		res.json({});
	})
	.post('/api/trays/users/:id', isAuthenticated, function (req, res) {
		// body
		res.json({});
	})
	.delete('/api/trays/users/:id', isAuthenticated, function (req, res) {
		// body
		res.json({});
	})
	.put('/api/trays/users/:id', isAuthenticated, function (req, res) {
		// body...
		res.json({});
	});


/* GET media likes . */
router
	.get('/api/trays/media/:id/like', isAuthenticated, function (req, res) {
  		res.json({});
	})
	.post('/api/trays/media/:id/like', isAuthenticated, function (req, res) {
		// body
		res.json({});
	})
	.delete('/api/trays/media/:id/like', isAuthenticated, function (req, res) {
		// body
		res.json({});
	})
	.put('/api/trays/media/:id/like', isAuthenticated, function (req, res) {
		// body...
		res.json({});
	});


/* GET media likes . */
router
	.get('/api/trays/media/:id/comments', isAuthenticated, function (req, res) {
  		res.json({});
	})
	.post('/api/trays/media/:id/comments', isAuthenticated, function (req, res) {
		// body
		res.json({});
	})
	.delete('/api/trays/media/:id/comments', isAuthenticated, function (req, res) {
		// body
		res.json({});
	})
	.put('/api/trays/media/:id/comments', isAuthenticated, function (req, res) {
		// body...
		res.json({});
	});



/* GET this week */
router
	.get('/api/weeks/:id', isAuthenticated, function (req, res) {
		// get a week
	})
	.get('/api/weeks', isAuthenticated, function (req, res) {
		// get the last 4 weeks
	});


module.exports = router;
