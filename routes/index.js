var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res) {
  res.render('index', { title: 'Welcome to tgif' });
});

router.get('/api', function (req, res) {
	res.render('api', { title: 'Welcome to tgif api'})
});

router.get('/token', function (req, res) {
	// get token for user to use this session
	res.json({});
});


module.exports = router;
