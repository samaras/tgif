var express = require('express');
var passport = require('passport');
var router = express.Router();

// User model
var User = require('../models/user');

/* GET users listing. */
router.get('/api/users/', function (req, res) {
	User.find(function(err, users){
		if(err)
			res.send(err);
		// return a list of uses
		res.json(users);
	});
  	res.send('respond with a resource');
});

/* GET login for user. */
router
	.get('/api/users/login', function (req, res) {
  		res.json({});
	})
	.post('/api/users/login', function (req, res) {
		// body
		res.json({});
	});


/* GET register for user. */
router
	.get('/api/users/register', function (req, res) {
  		res.json({});
	})
	.post('/api/users/register', function (req, res) {
		// body
		res.json({});
	});


/* GET user. */
router
	.get('/api/users/:id', isAuthenticated, function (req, res) {
  		User.find({ userId: req.user._id }, function(err, user){
  			if(err)
  				res.send(err);
  			res.json(user);
  		});  		
	})
	.post('/api/users', isAuthenticated, function (req, res) {
		var user = new User({
			username: req.body.username,
			password: req.body.password,
			email: req.body.email,
			first_name: req.body.first_name,
			last_name: req.body.last_name,
			picture: req.body.picture,
			status: req.body.status,
			profile_public: req.body.profile_public
		});

		user.save(function(err){
			if(err) 
				res.send(err);
		});
		res.json({message: 'New user added'});
	})
	.delete('/api/users/:id', isAuthenticated, function (req, res) {
		// body
		res.json({});
	})
	.put('/api/users/:id', isAuthenticated, function (req, res) {
		// body...
		res.json({});
	});


/* GET user notifications. */
router
	.get('/api/users/:id/notifications', isAuthenticated, function (req, res) {
  		res.json({});
	})
	.post('/api/users/:id/notifications', isAuthenticated, function (req, res) {
		// save users notification in db for push message
		res.json({});
	})
	.delete('/api/users/:id/notifications/:id', isAuthenticated, function (req, res) {
		// body
		res.json({});
	});


/* GET users followers . */
router
	.get('/api/users/:id/follows/', isAuthenticated, function (req, res) {
		// 
  		res.json({});
	})
	.post('/api/users/:id/follows', isAuthenticated, function (req, res) {
		// add a new follower to user with :id
		res.json({});
	})
	.delete('/api/users/:id/follows/:fid', isAuthenticated, function (req, res) {
		// user with :id unfollows user with :fid
		res.json({});
	})
	.put('/api/users/:id/follows/:fid', isAuthenticated, function (req, res) {
		// user with :id request to follow user with :fid
		res.json({});
	});


/* GET media for user. */
router
	.get('/api/users/media/:id', isAuthenticated, function (req, res) {
  		res.json({});
	})
	.post('/api/users/media', isAuthenticated, function (req, res) {
		// body
		res.json({});
	})
	.delete('/api/users/media/:id', isAuthenticated, function (req, res) {
		// body
		res.json({});
	})
	.put('/api/users/media/:id', isAuthenticated, function (req, res) {
		// body...
		res.json({});
	});


/* GET a list of users media stored */
router.get('/api/users/:id/media', isAuthenticated, function (req, res) {
	// body...
});


module.exports = router;
