var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var jwt = require('jwt-simple');

var routes = require('./routes/index');
var users = require('./routes/users');
var trays = require('./routes/trays');

// Get model
var User = require('./models/user');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(require('express-session')({
    secret: 'im not a cat, but you are',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.set('jwtTokenSecret', 'th@nk god its frid@y');

app.use('/', routes);
app.use('/users', users);

// passport config
var User = require('./models/user');

// passport.js Basic, uses plain text
passport.use(new BasicStrategy(
    function (username, password, done) {
        User.findOne({username: username}, function (err, user) {
            if (err) { return done(err); }
            
            // No user found with given username & password
            if (!user) { return done(null, false); }

            // Password compare
            user.comparePassword(password, function(err, isMatch){
                if(err)  { return done(null, false); }
                // Password failed
                if(!isMatch) { return done(err, false); }
                // Match
                return done(null, user);
            });
        });
    })
);

var isAuthenticated = passport.authenticate('basic', { session: false});

/*passport.use(new DigestStrategy({ qop: 'auth' },
    function (username, done) {
        User.findOne({ username:username }, function (err, user) {
            if (err) { return done(err); }
            if (!user) { return done(null, false); }
            return done(null, user, user.password);
        });
    },
    function (params, done) {
        done(null, true);
    }
));*/

// mongoose
mongoose.connect('mongodb://localhost/tgif');

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
