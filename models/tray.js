var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Tray = new Schema({
	user_id: int,
	week_id: int,
});

var TrayLikes = new Schema({
	tray_id: int,
	user_id: int,
	date_liked: Date,
});

var TrayComments = new Schema({
	tray_id: int,
	tray_comments: String,
	user_id: int,
	date_commented: Date,
	blocked: boolean,
});

var week = new Schema({
	week: String,
	date_from: Date,
	date_to: Date
});

module.exports = mongoose.model('trays', Tray);