"use strict";

var mongoose = require('mongoose'),
	bcrypt = require("bcryptjs"),
 	Schema = mongoose.Schema;

var UserSchema = new Schema({
	username: {
		type: String,
		unique: true,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	email: {
		type: String,
		unique: true
	},
	first_name: {
		type: String
	},
	last_name: { type: String },
	last_login: { type: Date},
	picture: { type: String },
	status: { type: String },
	profile_public: { type: Number },
	token: {
		type: String,
		unique: true
	}
});

// Execute before each user.save()
UserSchema.pre('save', function(callback) {
	var user = this;

	if(!user.isModified('password')) return callback();

	// On password changed, hash it
	bcrypt.genSalt(5, function(err, salt) {
		if(err) return callback(err);

		bcrypt.hash(user.password, salt, null, function(err, hash){
			if(err) return callback(err);
			user.password = hash;
			callback();
		})
	});
});

UserSchema.methods.comparePassword = function(passw, callback) {
	bcrypt.compare(passw, this.password, function(err, isMatch) {
		if(err) {
			return callback(err);
		} 
		callback(null, isMatch);
	});
};


/*
var Followers = new Schema({
	user_id: Number,
	follower_id: Number,
	allow: Number
});

var Media = new Schema({
	filename: String,
	title: String,
	type: String,
	size: String,
	is_video: Number
});

var UsersMedia = new Schema({
	media_id: Number,
	user_id: Number,
	date_published: Date,
	shared: Number
});*/

module.exports = mongoose.model('User', UserSchema);